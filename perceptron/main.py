# -*- coding: utf-8 -*-

import json
import pathlib
import timeit

import numpy as np
from matplotlib import pyplot as plt

from perceptron.data import load_data, shuffle_data
from perceptron.data.errors import save_errors, load_errors
from perceptron.nans.nan import zero_nans
from perceptron.perceptron import make_nn, learn, predict, set_weights, \
    mse_loss, make_output_buffer, to_one_row_mat
from perceptron.data.weights import save_weights, load_weights
from perceptron.perceptron.loss import _mse_loss, rmse_loss

WEIGHTS_DIR = 'weights'
WEIGHTS_FILE_NAME = 'weights.npz'

ERRORS_DIR = 'errors'
ERRORS_FILE_NAME = 'errors.json'

ERRORS_FILE = 'graph/errors.jpg'
TEST_APPROX_FILE = 'graph/test_approx.jpg'

KGF_APPROX_FILE = 'graph/kgf.jpg'
G_TOTAL_FILE = 'graph/g_total.jpg'

TRAIN_PARAMS_FILE = 'train_parameters.json'
CONFIG_FILE = 'config.json'


def load_config(config_file_name: str = CONFIG_FILE) -> dict[str, str]:
    with open(config_file_name) as config_file:
        return json.load(config_file)


def load_parameters(train_parameters_file_name: str = TRAIN_PARAMS_FILE) -> list[tuple[int, float]]:
    with open(train_parameters_file_name) as train_parameters_file:
        return list(map(
            lambda it: (it['epoch_count'], it['learning_rate']),
            json.load(train_parameters_file)))


def is_cached(weights_file_name: str, errors_file_name: str) -> bool:
    return pathlib.Path(weights_file_name).exists() and pathlib.Path(errors_file_name).exists()


def cache_files(i: int, epoch_count: int, learning_rate: float) -> (str, str):
    weights_file_name = f'{WEIGHTS_DIR}/{i}-{epoch_count}-{learning_rate}-{WEIGHTS_FILE_NAME}'
    errors_file_name = f'{ERRORS_DIR}/{i}-{epoch_count}-{learning_rate}-{ERRORS_FILE_NAME}'

    return weights_file_name, errors_file_name


def plot_results(
        nn: tuple[np.ndarray, ...],
        all_data: np.ndarray,
        all_target: np.ndarray,
        test_data: np.ndarray,
        test_target: np.ndarray,
        test_errors: list[float],
        train_errors: list[float]):
    # Plot errors
    fig, (ax1, ax2) = plt.subplots(ncols=2)

    ax1.plot(train_errors)
    ax1.set_title('Train loss (MSE)')

    ax2.plot(test_errors)
    ax2.set_title('Test loss (MSE)')

    plt.tight_layout()
    plt.savefig(ERRORS_FILE)

    # Plot test set approximation
    output = predict(nn, test_data, make_output_buffer(nn, test_data))
    data_points = list(range(len(test_data)))
    print(f'test_mse_loss={mse_loss(output, test_target)}')
    # print(f'test_rmse_loss={rmse_loss(output, test_target)}')

    plt.clf()

    g_total_rmse_error = rmse_loss(output[:, 0], test_target[:, 0])
    kgf_rmse_error = rmse_loss(output[:, 1], test_target[:, 1])

    print(f'КГФ RMSE: {kgf_rmse_error * 100:.4f}%')
    print(f'G_total RMSE: {g_total_rmse_error * 100:.4f}%')

    fig, (ax1, ax2) = plt.subplots(2)

    ax1.plot(data_points, output[:, 0], test_target[:, 0], linestyle='dashed', marker='o')
    ax1.set_title('G_total')
    ax1.legend(('Model output', 'Target'))

    ax2.plot(data_points, output[:, 1], test_target[:, 1], linestyle='dashed', marker='o')
    ax2.set_title('КГФ')
    ax2.legend(('Model output', 'Target'))

    fig.suptitle('Test')

    plt.tight_layout()
    plt.savefig(TEST_APPROX_FILE)

    # Approximate all data
    output = predict(nn, all_data, make_output_buffer(nn, all_data))
    data_points = list(range(len(all_data)))
    print(f'target_mse_loss={mse_loss(output, all_target)}')

    plt.clf()

    # Plot G_total approximation
    plt.plot(data_points, output[:, 0], all_target[:, 0], linestyle='dashed', marker='o')
    plt.title('G_total')
    plt.legend(('Model output', 'Target'))

    plt.tight_layout()
    plt.savefig(G_TOTAL_FILE)

    plt.clf()

    # Plot KGF approximation
    plt.plot(data_points, output[:, 1], all_target[:, 1])
    plt.title('КГФ')
    plt.legend(('Model output', 'Target'))

    plt.tight_layout()
    plt.savefig(KGF_APPROX_FILE)


def train(
        nn: tuple[np.ndarray, ...],
        train_data: np.ndarray,
        train_target: np.ndarray,
        test_data: np.ndarray,
        test_target: np.ndarray,
        train_errors: list[float],
        test_errors: list[float],
        epoch_count: int,
        learning_rate: float,
        single_row_outputs_buffer: tuple[np.ndarray, ...],
        train_output_buffer: tuple[np.ndarray, ...],
        test_output_buffer: tuple[np.ndarray, ...],
        weight_corrections_buffer: tuple[np.ndarray, ...]):
    for ind in range(epoch_count):
        learn(nn, train_data, train_target, learning_rate, single_row_outputs_buffer, weight_corrections_buffer)

        train_errors.append(mse_loss(predict(nn, train_data, train_output_buffer), train_target))
        test_errors.append(mse_loss(predict(nn, test_data, test_output_buffer), test_target))


def main(args: list[str]) -> None:
    config = load_config(args[0]) if args else load_config()
    train_parameters = load_parameters(config['train_parameters_file'])

    if config['shuffle_data']:
        shuffle_data(config['data_file'])
    (data, target), (train_data, train_target), (test_data, test_target) = load_data(config['data_file'])
    zero_nans(data)
    zero_nans(train_data)
    zero_nans(test_data)

    print(f'train_set_size={len(train_data)}')
    print(f'test_set_size={len(test_data)}')

    nn, weight_corrections_buffer = make_nn(train_data.shape[1], 47, 65, 2)

    if config['use_saved_initial_weights']:
        saved_weights = load_weights(config['weights_file'])
        set_weights(nn, saved_weights)
    else:
        save_weights(config['weights_file'], nn)

    single_row_outputs_buffer = make_output_buffer(nn, to_one_row_mat(train_data[0]))
    train_output_buffer = make_output_buffer(nn, train_data)
    test_output_buffer = make_output_buffer(nn, test_data)

    train_errors = []
    test_errors = []

    can_use_cached = True

    total_epoch_count = 0
    for i, (epoch_count, learning_rate) in enumerate(train_parameters):
        weights_file_name, errors_file_name = cache_files(i, epoch_count, learning_rate)

        if is_cached(weights_file_name, errors_file_name) and can_use_cached and config['use_cached_weights']:
            saved_weights, saved_errors = load_weights(weights_file_name), load_errors(errors_file_name)

            set_weights(nn, saved_weights)
            train_errors.extend(saved_errors['train'])
            test_errors.extend(saved_errors['test'])

            print(f'Using cached train results ({weights_file_name})')
        else:
            can_use_cached = False
            train_time = timeit.timeit(lambda: train(
                nn, train_data, train_target, test_data, test_target,
                train_errors, test_errors, epoch_count, learning_rate,
                single_row_outputs_buffer, train_output_buffer, test_output_buffer,
                weight_corrections_buffer), number=1)
            print(f'epoch_count={epoch_count} time={train_time:.2f}s')

            save_weights(weights_file_name, nn)
            save_errors(errors_file_name, train_errors[total_epoch_count:], test_errors[total_epoch_count:])

        total_epoch_count += epoch_count

    print(f'epoch_count={total_epoch_count}')

    plot_results(nn, data, target, test_data, test_target, train_errors, test_errors)
