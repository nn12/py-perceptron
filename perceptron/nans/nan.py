import numpy as np

from numba import njit


@njit(cache=True)
def zero_nans_jit(array2d: np.ndarray) -> None:
    for i in range(array2d.shape[0]):
        for j in range(array2d.shape[1]):
            if np.isnan(array2d[i][j]):
                array2d[i][j] = 0.0


def zero_nans(array: np.ndarray) -> None:
    array[np.isnan(array)] = 0.0
