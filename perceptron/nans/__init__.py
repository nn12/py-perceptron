from perceptron.nans.nan import zero_nans_jit, zero_nans


__all__ = ['zero_nans', 'zero_nans_jit']
