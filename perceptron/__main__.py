import timeit
import sys

from perceptron.main import main

execution_time = timeit.timeit(lambda: main(sys.argv[1:]), number=1)
print(f'total_execution_time={execution_time:.2f}s')
