import numpy as np

from numba import njit


@njit(cache=True)
def to_one_row_mat(array: np.ndarray) -> np.ndarray:
    return array.reshape(1, array.shape[0])


@njit(cache=True)
def append_ones_column(array: np.ndarray) -> np.ndarray:
    return np.concatenate(((np.ones(shape=(array.shape[0], 1))), array), axis=1)


@njit(cache=True)
def sigmoid(z: np.ndarray) -> np.ndarray:
    return 1.0 / (1.0 + np.exp(-z))


@njit(cache=True)
def copy_to(dst: np.ndarray, src: np.ndarray) -> None:
    n = len(dst)
    for i in range(n):
        dst[i] = src[i]
