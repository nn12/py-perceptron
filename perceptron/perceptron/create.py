import numpy as np
import math

from perceptron.perceptron.misc import copy_to


def make_layer(inputs_count: int, outputs_count: int, weight_index: int) -> np.ndarray:
    return np.random.uniform(
        0.01 * math.pow(0.2, weight_index),
        0.02 * math.pow(0.2, weight_index),
        size=(inputs_count, outputs_count))


def make_nn(*neurons_count) -> (tuple[np.ndarray, ...], tuple[np.ndarray, ...]):
    """
    :param neurons_count: numbers of neurons for each layer, from input to output
    :return: layers and weight correction buffers, inputs x outputs
    """
    layers_count: int = len(neurons_count) - 1
    layers: list[np.ndarray] = []
    weight_corrections: list[np.ndarray] = []

    weight_index = layers_count - 1

    for index in range(layers_count):
        inputs_count = neurons_count[index] + 1
        outputs_count = neurons_count[index + 1]

        layers.append(make_layer(inputs_count, outputs_count, weight_index))
        weight_corrections.append(np.empty(shape=(inputs_count, outputs_count)))

        weight_index -= 1

    return tuple(layers), tuple(weight_corrections[::-1])


def set_weights(nn: tuple[np.ndarray, ...], saved_weights_: tuple[np.ndarray, ...]) -> None:
    for i, weights in enumerate(saved_weights_):
        copy_to(nn[i], weights)


def make_output_buffer(layers: tuple[np.ndarray, ...], data: np.ndarray) -> tuple[np.ndarray, ...]:
    layers_count = len(layers)
    outputs = (np.empty(shape=data.shape),) + tuple(
        np.empty(shape=(data.shape[0], layers[i].shape[1])) for i in range(layers_count))
    return outputs
