import numpy as np

from numba import njit

from perceptron.perceptron.misc import append_ones_column, copy_to, to_one_row_mat
from perceptron.perceptron.predict import feed_forward
from perceptron.nans import zero_nans_jit


@njit(cache=True)
def weight_correction_last(
        output_layer_input: np.ndarray,
        output_layer_output: np.ndarray,
        target: np.ndarray) -> (np.ndarray, np.ndarray):
    # delta = sigmoid'(output) * err'(output, target)
    delta = output_layer_output * (1.0 - output_layer_output) * (output_layer_output - target)
    zero_nans_jit(delta)

    # delta, dw
    return delta, output_layer_input.T @ delta


@njit(cache=True)
def weight_correction(
        current_layer_input: np.ndarray,
        current_layer_output: np.ndarray,
        next_layer: np.ndarray,
        next_layer_delta: np.ndarray) -> (np.ndarray, np.ndarray):
    # sum(w_jk * delta_k)
    w_times_delta_sum = next_layer_delta @ next_layer.T
    # delta = sigmoid'(out) * sum(w_jk * delta_k)
    delta = w_times_delta_sum[:, 1:] * current_layer_output * (1.0 - current_layer_output)

    # delta, dw
    return delta, current_layer_input.T @ delta


@njit(cache=True)
def back_propagate(
        nn: tuple[np.ndarray, ...],
        target: np.ndarray,
        outputs_buffer: tuple[np.ndarray, ...],
        weight_corrections_buffer: tuple[np.ndarray]) -> None:
    """
    :param nn: [in] NN layers, inputs x outputs
    :param target: [in] train target
    :param outputs_buffer: [in, out] tuple of layer outputs
    :param weight_corrections_buffer: [in, out] tuple of layer weight corrections
    :return: None
    """
    layers_count = len(nn)

    # append ones bc bias
    output_layer_input = append_ones_column(outputs_buffer[-2])
    output_layer_output = outputs_buffer[-1]

    delta, correction = weight_correction_last(output_layer_input, output_layer_output, target)

    copy_to(weight_corrections_buffer[0], correction)
    i: int = 1

    for layer_index in range(2, layers_count + 1):
        current_layer_input = append_ones_column(outputs_buffer[-layer_index - 1])
        current_layer_output = outputs_buffer[-layer_index]

        delta, correction = weight_correction(
            current_layer_input, current_layer_output, nn[-layer_index + 1], delta)

        copy_to(weight_corrections_buffer[i], correction)
        i += 1


@njit(cache=True)
def learn(
        nn: tuple[np.ndarray, ...],
        train_data: np.ndarray,
        train_target: np.ndarray,
        learning_rate: float,
        outputs_buffer: tuple[np.ndarray, ...],
        weight_corr_buffer: tuple[np.ndarray]) -> None:
    """
    :param nn: [in, out] NN to be trained
    :param train_data: [in] training dataset inputs
    :param train_target: [in] training dataset targets
    :param learning_rate: [in] learning rate
    :param outputs_buffer: [in, out] tuple of layer outputs
    :param weight_corr_buffer: [in, out] tuple of layer weight correction values outputs
    :return: None
    """
    layers_count = len(nn)

    for i in range(len(train_data)):
        feed_forward(nn, to_one_row_mat(train_data[i]), outputs_buffer)
        back_propagate(nn, to_one_row_mat(train_target[i]), outputs_buffer, weight_corr_buffer)

        idx = len(weight_corr_buffer) - 1
        for j in range(layers_count):
            copy_to(nn[j], nn[j] - weight_corr_buffer[idx] * learning_rate)
            idx -= 1
