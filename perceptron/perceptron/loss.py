import numpy as np

from numba import njit

from perceptron.nans import zero_nans_jit, zero_nans


@njit(cache=True)
def mse_loss(output: np.ndarray, target: np.ndarray) -> float:
    diff = output - target
    zero_nans_jit(diff)
    return np.mean(diff ** 2)


def _mse_loss(output: np.ndarray, target: np.ndarray) -> float:
    diff = output - target
    zero_nans(diff)
    return np.mean(diff ** 2)


def rmse_loss(output: np.ndarray, target: np.ndarray) -> float:
    return _mse_loss(output, target) ** 0.5
