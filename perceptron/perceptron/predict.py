import numpy as np

from numba import njit

from perceptron.perceptron.misc import sigmoid, copy_to, append_ones_column


@njit(cache=True)
def apply_layer(layer: np.ndarray, data: np.ndarray) -> np.ndarray:
    """
    :param layer: inputs x outputs
    :param data: 1 x inputs
    :return: 1 x outputs
    """
    return sigmoid(data @ layer)


@njit(cache=True)
def feed_forward(
        layers: tuple[np.ndarray, ...],
        data: np.ndarray,
        outputs: tuple[np.ndarray, ...]) -> None:
    """
    :param layers:  [in] tuple of NN layers from input to output, inputs x outputs
    :param data:    [in] 2d array of data rows, nrows x inputs
    :param outputs: [out] tuple of layer outputs
    :return: None
    """
    copy_to(outputs[0], data)
    for i, layer in enumerate(layers):
        copy_to(outputs[i + 1], apply_layer(layer, append_ones_column(outputs[i])))


@njit(cache=True)
def predict(nn: tuple[np.ndarray, ...], data: np.ndarray, outputs_buffer: tuple[np.ndarray]) -> np.ndarray:
    """
    :param nn: [in] NN layers, inputs x outputs
    :param data: [in] 2d array of data rows, nrows x inputs
    :param outputs_buffer: [in, out] tuple of layer outputs
    :return: 2d array of outputs corresponding to data
    """
    feed_forward(nn, data, outputs_buffer)
    return outputs_buffer[-1]
