import json


def load_errors(errors_file_name: str) -> dict[str, list[float]]:
    with open(errors_file_name) as errors_file:
        return json.load(errors_file)


def save_errors(errors_file_name: str, train_errors: list[float], test_errors: list[float]) -> None:
    data = {
        'test': test_errors,
        'train': train_errors
    }
    with open(errors_file_name, 'w') as errors_file:
        return json.dump(data, errors_file)
