import numpy as np


def load_weights(file_name: str) -> tuple[np.ndarray, np.ndarray, np.ndarray]:
    layers_dict = np.load(file_name)
    return layers_dict['l1'], layers_dict['l2'], layers_dict['l3']


def save_weights(file_name: str, layers: tuple[np.ndarray, np.ndarray, np.ndarray]) -> None:
    l1, l2, l3 = layers
    np.savez(file_name, l1=l1, l2=l2, l3=l3)
