import csv
import numpy as np
import random

DATA_FILE = 'data/data.csv'


def normalize(array, array_max, array_min, high, low):
    return (array - array_min) / (array_max - array_min) * (high - low) + low


def shuffle_data(data_file_name: str = DATA_FILE) -> None:
    if data_file_name != DATA_FILE:
        raise ValueError(f'"{data_file_name}" must not be edited')
    with open(data_file_name) as data_file:
        lines = list(filter(lambda it: it.strip(), data_file.readlines()))
    random.shuffle(lines)
    with open(data_file_name, 'w') as data_file:
        data_file.write(''.join(lines))


def load_data(
        data_file_name: str = DATA_FILE,
        train_weight: float = 0.6,
        shuffle: bool = False) -> (tuple[np.ndarray, np.ndarray],
                                   tuple[np.ndarray, np.ndarray],
                                   tuple[np.ndarray, np.ndarray]):
    with open(data_file_name, encoding='UTF-8') as data_file:
        reader = csv.reader(data_file, delimiter=',')
        rows = list(reader)

        data = np.asanyarray(rows, dtype=float)
        dataset: np.ndarray = data[:, :-2]
        target: np.ndarray = data[:, -2:]

        kgf_max = np.nanmax(target[:, -1:])
        kgf_min = np.nanmin(target[:, -1:])

        gtotal_max = np.nanmax(target[:, :-1])
        gtotal_min = np.nanmin(target[:, :-1])

        target[:, 0] = normalize(target[:, 0], gtotal_max, gtotal_min, 1, 0)
        target[:, 1] = normalize(target[:, 1], kgf_max, kgf_min, 1, 0)

        data_indices = list(range(dataset.shape[0]))
        if shuffle:
            random.shuffle(data_indices)

        train_size = round(len(data_indices) * train_weight)
        train_indices = data_indices[:train_size]
        test_indices = data_indices[train_size:]

    dataset = dataset[data_indices[:]]
    target = target[data_indices[:]]

    train_data = dataset[train_indices].copy()
    train_target = target[train_indices].copy()

    test_data = dataset[test_indices].copy()
    test_target = target[test_indices].copy()

    return (dataset, target), \
           (train_data, train_target), \
           (test_data, test_target)
